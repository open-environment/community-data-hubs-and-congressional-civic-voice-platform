Summary: Build systems, out of existing parts, that support community environmental sensemaking through new data governance structures in addition to supporting the flow of that information through existing governance systems that go up in scale (local, state, federal, tribal). To note, this idea is somewhat the centrifuge of the environmental trend ecosystem.

What’s the idea?
Neither of these two platforms will be a stand alone piece of technology, instead they will be composed of different pieces of open source infrastructure, building something new out of existing open parts and documenting the systems for others to replicate.

Combined, both platforms and the systems that interoperate between them, provide a model for investigating ways to balance models of decentralization and federalization within the universe of environmental data.

Both products will share technical elements that allow for the exchange of data relevant to the user group needs. The institutional requirements for which the CVA will be built around are first, and foremost, changing at this moment in time, but are likely the most rigid and are enforced through a complex body of law. 

The environmental trend platform will be a model that represents new community data governance structures which will be designed around community needs which are often dismissed in larger data infrastructures. These governance features would include ways to encode community “epistemic” innovations in understanding environmental risk and traditional knowledge tagging systems which test new socio-technical features around sensitivity and privacy. In addition the novel technical side will be centered around harmonizing multiple data streams from both public and private sources for a complete environmental picture.

Both models are designed to be decentralized (in the community instance) or federalized (in the congressional district instance). This allows for the power of local ownership over information flows, but takes advantage of the increasing power of networked data. Our goal is to create networked data streams from both platforms that enable sensemaking, and decision-making which is timely, relevant to the issue and community, and trustworthy.

The larger narrative we are sewing together with these platforms, and will promote, is that community provided data and the tools they use to provide this data are valid forms of knowing across all governance scales. They require technical and social knowledge transfer work (for instance humans that can translate a community's definition of risk into a government system assessment of risk and encode that into the technical platform). This type of deep community engagement is often difficult to do and largely a performative action on behalf of elected officials versus a meaningful learning exchange. Through embracing and using this data we can provide a place for environmental sensemaking that gives communities, and either their community leaders and/or elected officials, an opportunity to respond to difficult environmental trade-off decisions with trustworthy, contextual and relevant information.

Examples/Use Cases:
- National Environmental justice mapping tool - include community risk categories/vocabulary in the methodology for assessing categorical risk - see HR 516 env justic mapping tool development. 
